from docx import Document
import string    
import random 
import sys
import json
S = 10    

document = Document('surat.docx')

inputarray = sys.argv[1]

dic = json.loads(inputarray)


for p in document.paragraphs:
    inline = p.runs
    for i in range(len(inline)):
        text = inline[i].text
        for key in dic.keys():
            if key in text:
                 text=text.replace(key,dic[key])
                 inline[i].text = text

for table in document.tables:
    for row in table.rows:
        for cell in row.cells:
            for p in cell.paragraphs:
                inline = p.runs
                for i in range(len(inline)):
                    text = inline[i].text
                    for key in dic.keys():
                        if key in text:
                            text=text.replace(key,dic[key])
                            inline[i].text = text
                        

ran = ''.join(random.choices(string.ascii_uppercase + string.digits, k = S)) 

document.save('files/'+str(ran)+'.docx')
print('done')
